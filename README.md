### Tricks in Swift Xcode
# Create a marker or pin on the map - iOS Swift Xcode 
An iOS application shows how to create a marker or pin on the map - iOS Swift Xcode Version 7.3.1
<br>
![Marker Pin Maps Swift](https://gitlab.com/troy.borges/Tricks-in-Swift-Xcode-Version-7-3-1/-/raw/master/Screenshot.png)
